<?php

//header('HTTP/1.1 500 Internal Server Error', true, 500);
//exit;

use Phalcon\Mvc\Application;
use Phalcon\Config\Adapter\Ini as ConfigIni;

try {

	define('APP_PATH', realpath('..') . '/');

	/**
	 * Read the configuration
	 */
	$config = new ConfigIni(APP_PATH . 'app/config/config.ini');

	/**
	 * Load application services
	 */
	require APP_PATH . 'app/config/services.php';

    $application = new Application($di);

    // Регистрация установленных модулей
    $application->registerModules(
        array(
            'api'  => array(
                'className' => 'Api\Module',
                'path'      => APP_PATH . 'app/modules/api/Module.php',
            )
        )
    );

	echo $application->handle()->getContent();

} catch (Exception $e) {
     echo "Exception: ", $e->getMessage();
}
