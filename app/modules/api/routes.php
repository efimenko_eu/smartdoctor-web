<?php

// API
$router->addPost(
    "/api/v1/login",
    array(
        "module"		=> "api",
        "controller" 	=> "login",
        "action"     	=> "index"
    )
);
$router->add(
    "/api/v1/doctors",
    array(
        "module"		=> "api",
        "controller" 	=> "doctors",
        "action"     	=> "index"
    )
);