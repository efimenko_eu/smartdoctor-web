<?php

use Phalcon\Logger\Adapter\File as FileAdapter;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class ChatTask extends \Phalcon\CLI\Task {

    public function mainAction() {

        $logger = new FileAdapter(APPLICATION_PATH . "/logs/errors.log");

        try {
            require APPLICATION_PATH . '/libraries/Chat/Chat.php';

            $server = IoServer::factory(
                new HttpServer(
                    new WsServer(
                        new Chat()
                    )
                ),
                (int) $this->config->socket->port,
                (string) $this->config->socket->ip
            );

            $server->run();
        } catch (\Exception $e) {

            $logger->error("--ERROR--");
            $logger->error($e->getMessage());
            $logger->error($e->getFile());
            $logger->error($e->getLine());
            $logger->error($e->getTraceAsString());
        }

    }

}