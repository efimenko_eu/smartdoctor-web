<?php
/**
 * Created by Fedor Efimenko.
 * Email: fedor@efimenko.eu
 * Date: 10/14/17
 */

namespace Api\Model;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Messages extends Model {

    public $id;
    public $user_from;
    public $user_to;
    public $message;
    public $time;

    public function getSource() {
        return "Messages";
    }

}