<?php

namespace Api\Model;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Users extends Model {

	public $id;
	public $soc_type;
    public $soc_id;
    public $spec_id;
    public $type;
    public $name;
    public $avatar;

  	public function getSource() {
      	return "Users";
  	}

  	public function validation() {

  		$validator = new Validation();

//	    $validator->add(
//	        'username',
//	        new Uniqueness([
//				'message' => "Already exist"
//			])
//	    );

    	return $this->validate($validator);

    }

}
