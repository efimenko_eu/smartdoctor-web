<?php

namespace Api;

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\DiInterface;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface {
    /**
     * Регистрация автозагрузчика, специфичного для текущего модуля
     */
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null) {
        $loader = new Loader();

        $loader->registerNamespaces(
            array(
                'Api\Controller' => APP_PATH . 'app/modules/api/controllers/',
                'Api\Model'      => APP_PATH . 'app/modules/api/models/',
                'Firebase\JWT'      => APP_PATH . 'app/libraries/Firebase/'
            )
        );

        $loader->register();
    }

    /**
     * Регистрация специфичных сервисов для модуля
     */
    public function registerServices(DiInterface $di) {

        $di->set('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("Api\Controller");
            return $dispatcher;
        });

    }
}