<?php
/**
 * Created by Fedor Efimenko.
 * Email: fedor@efimenko.eu
 * Date: 10/13/17
 */

namespace Api\Controller;

use Api\Model\Users;

class DoctorsController extends \Api\Controller\ControllerBase {

    public function indexAction() {

        $response = new \Phalcon\Http\Response();

        $category_id = $this->request->getPost("category_id", "int");

        if($category_id == 0) {
            $phql = "SELECT Users.* FROM \Api\Model\Users as Users
				WHERE Users.type = 1";
            $doctors = $this->modelsManager->executeQuery($phql, array(
                //"spec_id" => $category_id
            ));
        } else {
            $phql = "SELECT Users.* FROM \Api\Model\Users as Users
				WHERE Users.type = 1 AND Users.spec_id = :spec_id:";
            $doctors = $this->modelsManager->executeQuery($phql, array(
                "spec_id" => $category_id
            ));
        }

        $response->setJsonContent(array(
            "status"    => true,
            "doctors" => $doctors->toArray(),
            "doc" => Users::find()->toArray()
        ));
        return $response;

    }

}