<?php

namespace Api\Controller;

use Api\Model\Specializations;
use Phalcon\Mvc\Controller;
use \Api\Model\Users;

class LoginController extends Controller {

    public function indexAction() {

        $response = new \Phalcon\Http\Response();

        // Check social type
        $soc_type = $this->request->getPost("soc_type");
        if(!in_array($soc_type, ["fb"])) {
            $response->setJsonContent([
                "status" => false,
                "message" => "Incorrect social type"
            ]);
            $response->setStatusCode(404, "Not Found");
            return $response;
        }

        $soc_token = $this->request->getPost("soc_token");
        $soc_id = -1;

        // Check social token [fb]
        $fb = json_decode(@file_get_contents("https://graph.facebook.com/me?access_token=" . $soc_token . "&fields=name,picture.type(large)"));
        if(!isset($fb->id)) {
            $response->setJsonContent([
                "status" => false,
                "message" => "Incorrect social id"
            ]);
            $response->setStatusCode(404, "Not Found");
            return $response;
        } else {
            $soc_id = $fb->id;
        }

        // Check account type
        $type = $this->request->getPost("type", "int");
        if(!in_array($type, [0, 1])) {
            $response->setJsonContent([
                "status" => false,
                "message" => "Incorrect account type"
            ]);
            $response->setStatusCode(404, "Not Found");
            return $response;
        }


        // Check exist user
        $phql = "SELECT Users.* FROM \Api\Model\Users as Users
				WHERE Users.soc_id = :soc_id: AND Users.soc_type = :soc_type: AND Users.type = :type:";
        $user = $this->modelsManager->executeQuery($phql, array(
            "soc_id" => $soc_id,
            "soc_type" => $soc_type,
            "type" => $type,
        ))->getFirst();

        if(!$user) {
            $user = new Users();
            $user->soc_id   = $fb->id;
            $user->soc_type = $soc_type;
            $user->type     = $type;
            $user->name     = $fb->name;
            $user->avatar   = $fb->picture->data->url;

            if (!$user->save()) {
                $errors = array();
                foreach ($user->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }

                $response->setJsonContent(array('status' => false, 'message' => implode(", ", $errors)));
                $response->setStatusCode(401, "User didn't create");
                return $response;
            }

            $user->id = (int) $user->id;
            $user->type = (int) $user->type;
        }

        $jwt = \Firebase\JWT\JWT::encode(
            array(
                'id'    => $user->id,
                'type'  => $type
            ),
            $this->config->application->secret
        );

        $response->setJsonContent(array(
            "status"    => true,
            "user"      => $user->toArray(),
            "token"     => $jwt
        ));
        return $response;

    }

}