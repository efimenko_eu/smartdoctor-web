<?php

namespace Api\Controller;

use Phalcon\Mvc\Controller;

class ErrorsController extends Controller {

    public function show401Action() {
        $response = new \Phalcon\Http\Response();
        $response->setJsonContent(array(
            "status" => false,
            "message" => "401 error"
        ));
        $response->setStatusCode(401);
        return $response;
    }

    public function show404Action() {
        $response = new \Phalcon\Http\Response();
        $response->setJsonContent(array(
            "status" => false,
            "message" => "404 error"
        ));
        $response->setStatusCode(404);
        return $response;
    }

    public function show500Action() {
        $response = new \Phalcon\Http\Response();
        $response->setJsonContent(array(
            "status" => false,
            "message" => "500 error"
        ));
        $response->setStatusCode(500);
        return $response;
    }

}