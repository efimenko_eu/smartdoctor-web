<?php
/**
 * Created by Fedor Efimenko.
 * Email: fedor@efimenko.eu
 * Date: 10/14/17
 */

use Phalcon\DI\Injectable as DIInjectable;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Logger;

class Chat extends DIInjectable implements Ratchet\MessageComponentInterface {

    private $e;

    public function errorHandler($severity, $message, $file, $line) {

        var_dump($message);

        if(mb_strpos($message, "Ratchet\Server\IoConnection::") == false) {
            var_dump("EXCEPTION");
            throw new ErrorException($message, $severity, $severity, $file, $line);
        }

    }

    public function __construct() {

        $this->clients = new \SplObjectStorage;
        $this->e = new Phalcon\Escaper();

        echo "Chat server started!\n";

        error_reporting(E_ALL);
        set_error_handler(array($this, "errorHandler"));
    }

    public function onOpen(Ratchet\ConnectionInterface $conn) {

        echo "New connection! ({$conn->resourceId})\n";
        $this->clients->attach($conn);

    }

    public function onMessage(Ratchet\ConnectionInterface $from, $msg) {

        try {

            echo $msg;
            $json = json_decode($msg);



        } catch (Exception $e) {
            echo "Exception CHAT: ", $e->getMessage();
            $logger = new FileAdapter(APPLICATION_PATH . "/logs/errors.log");
            $logger->log("--ERROR--", Logger::INFO);
            $logger->log($e->getMessage(), Logger::INFO);
            $logger->log($e->getFile(), Logger::INFO);
            $logger->log($e->getLine(), Logger::INFO);
            $logger->log($e->getTraceAsString(), Logger::INFO);
        }

    }

    public function onClose(Ratchet\ConnectionInterface $conn) {



    }

    public function onError(Ratchet\ConnectionInterface $conn, \Exception $e) {

        $logger = new FileAdapter(APPLICATION_PATH . "/logs/errors.log");
        $logger->log("--ERROR--", Logger::INFO);
        $logger->log($e->getMessage(), Logger::INFO);
        $logger->log($e->getFile(), Logger::INFO);
        $logger->log($e->getLine(), Logger::INFO);
        $logger->log($e->getTraceAsString(), Logger::INFO);

        $conn->send(json_encode(array(
            'status' => false,
            'message' => 'An error has occurred: '.$e->getMessage()
        )));
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();

    }

}