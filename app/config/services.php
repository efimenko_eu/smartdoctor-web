<?php

use Phalcon\Mvc\View;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Security;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Config\Adapter\Ini as ConfigIni;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('router', function(){
    require_once( APP_PATH . 'app/config/routes.php' );
    return $router;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function() use ($config){
	$url = new UrlProvider();
	$url->setBaseUri("/");
	return $url;
}, true);

$di->set('view', function() use ($config) {
	$view = new View();
	$view->registerEngines(array(
		".volt" => 'volt'
	));
	return $view;
});

/**
 * Setting up volt
 */
$di->set('volt', function($view, $di) {
	$volt = new VoltEngine($view, $di);
	$volt->setOptions(array(
		"compiledPath" => APP_PATH . "cache/volt/"
	));
	return $volt;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function() use ($config) {
	return new PdoMysql(array(
		"host"     => $config->database->host,
		"username" => $config->database->username,
		"password" => $config->database->password,
        "port"     => $config->database->port,
        "dbname"   => $config->database->dbname,
        "options" => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_STRINGIFY_FETCHES => false,
        )
	));
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function() {
	$session = new SessionAdapter();
	$session->start();
	return $session;
});

/**
 *	Security
 */
$di->set('security', function () {
    $security = new Security();
    // Устанавливаем фактор хеширования в 12 раундов
    $security->setWorkFactor(12);
    return $security;
}, true);

/* Config */
$di->set('config', $config);

/**
 * Register the flash service with custom CSS classes
 */
$di->set('flash', function(){
	return new FlashSession(array(
		'error'   => 'alert alert-danger',
		'success' => 'alert alert-success',
		'notice'  => 'alert alert-info',
	));
});