<?php

use Phalcon\Mvc\Router;

// Создание маршрутизатора
$di = new \Phalcon\DI\FactoryDefault();
$router = new Router(false);
$router->setDI($di);

require_once( APP_PATH . 'app/modules/api/routes.php' );

$router->notFound(array(
    'module' 		=> 'api',
    'controller' 	=> 'errors',
    'action'     	=> 'show404',
));

$router->handle();