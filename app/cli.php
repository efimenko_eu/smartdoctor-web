<?php

use Phalcon\DI\FactoryDefault\CLI as CliDI,
    Phalcon\CLI\Console as ConsoleApp,
	Phalcon\Config\Adapter\Ini as ConfigIni,
	Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql,
	Phalcon\Mvc\View\Simple as SimpleView,
	Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use \Rollbar\Rollbar;

// Определяем путь к каталогу приложений
define('APPLICATION_PATH', realpath(dirname(__FILE__)));

// Используем стандартный для CLI контейнер зависимостей
$di = new CliDI();

// Загружаем файл конфигурации
$config = new ConfigIni(APPLICATION_PATH . '/config/config.ini');
$di->set('config', $config);

// Подключаем базу данных
$di->set('db', function() use ($config) {
	return new PdoMysql(array(
		"host"     => $config->database->host,
		"username" => $config->database->username,
		"password" => $config->database->password,
		"dbname"   => $config->database->dbname,
        'port'     => $config->database->port,
        "options" => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_EMULATE_PREPARES => false
        )
	));
});

/**
 * Регистрируем автозагрузчик, и скажем ему, чтобы зарегистрировал каталог задач
 */
$loader = new \Phalcon\Loader();
$loader->registerDirs([
    APPLICATION_PATH . '/modules/api/tasks/'
]);
$loader->registerNamespaces([
    'Api\Model'      	=> APPLICATION_PATH . '/modules/api/models/',
    'Firebase\JWT' 		=> APPLICATION_PATH . '/libraries/Firebase/'
]);
$loader->registerFiles([
    APPLICATION_PATH . '/vendor/autoload.php'
]);
$loader->register();

// Создаем консольное приложение
$console = new ConsoleApp();
$console->setDI($di);

/**
 * Определяем консольные аргументы
 */
$arguments = array();
foreach ($argv as $k => $arg) {
    if ($k == 1) {
        $arguments['task'] = $arg;
    } elseif ($k == 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments['params'][] = $arg;
    }
}

// определяем глобальные константы для текущей задачи и действия
define('CURRENT_TASK',   (isset($argv[1]) ? $argv[1] : null));
define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

try {
    // обрабатываем входящие аргументы
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    echo "exception CLI: " . $e->getMessage();
    exit(255);
}